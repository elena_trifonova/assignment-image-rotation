#include "image_format.h"
#include <malloc.h>

//function for initialization of image
void init_image(struct image* img, uint64_t width, uint64_t height) {
    uint64_t data_size = width * height * sizeof(struct pixel);
    img->width = width;
    img->height = height;
    img->data = (struct pixel*) malloc(data_size);
}

//function for cleaning image
void free_image(struct image* img) {
    img->width = 0;
    img->height = 0;
    free(img->data);
    img->data = NULL;
}

