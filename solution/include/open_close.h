#ifndef FILE_OPEN_CLOSE
#define FILE_OPEN_CLOSE
#include <stdio.h>

enum read_file_status {
    READ_OK = 0,
    OPEN_READ_ERROR
};

enum write_file_status {
    WRITE_OK = 0,
    OPEN_WRITE_ERROR
};

enum close_file_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum read_file_status read_file(FILE** file, const char* filepath);
enum write_file_status write_file(FILE** file, const char* filepath);
enum close_file_status close_file(FILE* file);

enum read_file_status print_file_rs(enum read_file_status status);
enum write_file_status print_file_ws(enum write_file_status status);
enum close_file_status print_file_cs(enum close_file_status status);
#endif

