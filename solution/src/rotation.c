#include "rotation.h"
#include <stdlib.h>

//function for rotating image on 90 deg
struct image image_rotate(struct image const* inner_img ) {
    struct image out = {0};
    init_image(&out, inner_img->height, inner_img->width);
    for (size_t i = 0; i < out.height; i++) {
        for (size_t j = 0; j < out.width; j++) {
            out.data[i * out.width + j] =  inner_img->data[(inner_img->height - j - 1) * inner_img->width + i];
        }
    }
    return out;
}

