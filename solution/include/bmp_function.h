#ifndef BMP_FORMAT_FUNC
#define BMP_FORMAT_FUNC
#include "image_format.h"
#include <stdio.h>


enum write_bmp_status {
    WRITE_BMP_OK = 0,
    WRITE_INVALID_INPUT,
    WRITE_ERROR
};

enum read_bmp_status {
    READ_BMP_OK = 0,
    READ_INVALID_FILE_PATH,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PLANES,
    READ_INVALID_COMPRESSION,
    READ_INVALID_RESERVED,
    READ_ERROR
};

enum read_bmp_status from_bmp( FILE* in, struct image* img );
enum write_bmp_status to_bmp( FILE* out, struct image const* img );

enum read_bmp_status print_bmp_rs(enum read_bmp_status status);
enum write_bmp_status print_bmp_ws(enum write_bmp_status status);
#endif

