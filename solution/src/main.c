#include "bmp_function.h"
#include "image_format.h"
#include "open_close.h"
#include "rotation.h"
#include <stdio.h>

int main( int argc, char** argv ) {

    (void) argc; (void) argv;

    if (argc != 3) {
        fprintf(stderr,"%s", "Usage: ./image-transformer <source-image> <transformed-image>");
        return 0;
    }

    FILE* inner_img = NULL;
    FILE* rotated_img = NULL;

    const char* inner_img_path = argv[1];
    const char* rotated_img_path = argv[2];

    struct image img = {0};


    enum read_file_status readf_status = read_file(&inner_img, inner_img_path);
    if (print_file_rs(readf_status)) return readf_status;


    enum read_bmp_status readbmp_status = from_bmp(inner_img, &img);
    if (print_bmp_rs(readbmp_status)) return readbmp_status;

    struct image img_rotated = image_rotate( &img);

     enum write_file_status writef_status = write_file(&rotated_img, rotated_img_path);
     if (print_file_ws(writef_status)) return writef_status;


     enum write_bmp_status writebmp_status = to_bmp(rotated_img, &img_rotated);
     if (print_bmp_ws(writebmp_status)) return writebmp_status;


     enum close_file_status close_writef_status = close_file(rotated_img);
     if(print_file_cs(close_writef_status)) return close_writef_status;


     enum close_file_status close_readf_status = close_file(inner_img);
     if(print_file_cs(close_readf_status)) return close_readf_status;

     free_image(&img);
     free_image(&img_rotated);


    return 0;
}

