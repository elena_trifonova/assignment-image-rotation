#include "open_close.h"
#include <stdio.h>

//function for opening for reading file
enum read_file_status read_file(FILE** file, const char* filepath){
    *file = fopen(filepath, "rb");
    if (!*file) return OPEN_READ_ERROR;
    return READ_OK;
}

//function for opening for writing file
enum write_file_status write_file(FILE** file, const char* filepath){
    *file = fopen(filepath, "wb");
    if (!*file) return OPEN_WRITE_ERROR;
    return WRITE_OK;
}

//function for closing file
enum close_file_status close_file(FILE* file) {
    if(file == NULL){
        return CLOSE_ERROR;
    }
    fclose(file);
    return CLOSE_OK;
}

//status description
static char* const rs_decoder[] = {
        [READ_OK] =             "File was opened successfully\n",
        [OPEN_READ_ERROR] =          "Error: File wasn't opened for reading, sth is wrong\n"
};

//status description
static char* const ws_decoder[] = {
        [WRITE_OK] =             "File was opened successfully\n",
        [OPEN_WRITE_ERROR] =          "Error: File wasn't opened for writing, sth is wrong\n"
};

//status description
static char* const cs_decoder[] = {
        [CLOSE_OK] =        "File was closed successfully\n",
        [CLOSE_ERROR] =     "Error: File wasn't closed\n"
};

//function for printing status
enum read_file_status print_file_rs(enum read_file_status status) {
    printf("%s", rs_decoder[status]);
    return status;
}

//function for printing status
enum write_file_status print_file_ws(enum write_file_status status) {
    printf("%s", ws_decoder[status]);
    return status;
}

//function for printing status
enum close_file_status print_file_cs(enum close_file_status status) {
    printf("%s", cs_decoder[status]);
    return status;
}

