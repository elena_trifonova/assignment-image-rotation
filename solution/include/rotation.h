#ifndef ROTATE_90
#define ROTATE_90
#include "image_format.h"

struct image image_rotate(struct image const* inner_img);
#endif
