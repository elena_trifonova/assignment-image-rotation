#include "bmp_function.h"
#include <stdint.h>
#include <stdio.h>

struct  __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};


static const uint32_t TYPE = 0x4D42;
static const uint32_t RESERVED = 0;
static const uint32_t SIZE = 40;
static const uint16_t PLANES = 1;
static const uint16_t BIT_COUNT = 24;
static const uint32_t COMPRESSION = 0;
static const uint32_t PIX_PER_METER = 0;
static const uint32_t CLR_USED = 0;
static const uint32_t CLR_IMPORTANT = 0;

//function for validation checking of bmp_header (bmp has special settings and we can find them on Internet)
static enum read_bmp_status check_header(const struct bmp_header header) {

    if (header.bfType !=TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.bfReserved != RESERVED){
        return READ_INVALID_RESERVED;
    }
    if (header.biBitCount != BIT_COUNT) {
        return READ_INVALID_BITS;
    }
    if (header.biPlanes != PLANES) {
        return READ_INVALID_PLANES;
    }
    if (header.biCompression != COMPRESSION) {
        return READ_INVALID_COMPRESSION;
    }
    if (header.bfileSize != header.biSizeImage + header.bOffBits || header.biSize != SIZE) {
        return READ_INVALID_HEADER;
    }

    return READ_BMP_OK;
}


//function for initialization of bmp_header
static struct bmp_header init_header(struct image const* img) {
    struct bmp_header header = {
        .bfType = TYPE,
        .bfileSize = sizeof(struct bmp_header) +
                     img->height * img->width * sizeof(struct pixel) +
                     img->height * img->width  % 4,
        .bfReserved = RESERVED,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = COMPRESSION,
        .biSizeImage =  img->height * img->width * sizeof(struct pixel) + img->height * img->width  % 4,
        .biXPelsPerMeter = PIX_PER_METER,
        .biYPelsPerMeter = PIX_PER_METER,
        .biClrUsed = CLR_USED,
        .biClrImportant = CLR_IMPORTANT,
    };
    return header;
}
//function for reading bmp_file
static enum read_bmp_status read_data(FILE* in, struct image const* img) {
    uint8_t padding = img->width  % 4;
    for (size_t i = 0; i < img->height; i++){
        if (fread(&(img->data[i*img->width]), sizeof(struct pixel), img->width, in)!= img->width) {
            return READ_ERROR;
        }
        if(fseek(in, padding, SEEK_CUR)){
            return READ_ERROR;
        }
    }
    return READ_BMP_OK;
}

//function for writing in bmp_file
static enum write_bmp_status write_data(FILE* out, struct image const* img) {
    uint8_t padding = img->width  % 4;
    const uint64_t zero = 0;
    for(size_t i = 0; i < img->height; i++) {
        if(fwrite(&(img->data[i*img->width]), sizeof(struct pixel), img->width, out) != img->width){
            return WRITE_ERROR;
        }
        if(!fwrite(&zero, 1, padding, out)){
            return WRITE_ERROR;
        }
    }
    return WRITE_BMP_OK;
}
//function for converting from bmp file
enum read_bmp_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;
    if (in == NULL) return READ_INVALID_FILE_PATH;

    fread(&header, sizeof(struct bmp_header), 1, in);

    enum read_bmp_status header_status = check_header(header);
    if (header_status != READ_BMP_OK)
        return header_status;

    init_image(img, header.biWidth, header.biHeight);

    enum read_bmp_status img_rs = read_data(in, img);

    return img_rs;
}

//function for converting to bmp file
enum write_bmp_status to_bmp( FILE* out, struct image const* img ) {
    if(out == NULL) return WRITE_INVALID_INPUT;

    struct bmp_header header = init_header(img);
    fwrite(&header, sizeof(struct bmp_header), 1, out);

    enum write_bmp_status img_ws = write_data(out, img);
    return img_ws;
}

//description of status
static char* const rs_decoder[] = {
        [READ_BMP_OK] =                     "Picture read successfully\n",
        [READ_INVALID_FILE_PATH] =      "Error: filepath is incorrect\n",
        [READ_INVALID_SIGNATURE] =      "Error: invalid signature of bmp_header\n",
        [READ_INVALID_BITS] =           "Error: wrong number of bits in bmp_header\n",
        [READ_INVALID_COMPRESSION] =    "Error: incorrect compression in bmp_header\n",
        [READ_INVALID_PLANES] =         "Error: invalid number of planes in bmp_header\n",
        [READ_INVALID_HEADER] =         "Error: invalid header\n",
        [READ_INVALID_RESERVED] =       "Error: invalid reserved space\n"
};

//function for printing status
enum read_bmp_status print_bmp_rs(enum read_bmp_status status) {
    printf("%s", rs_decoder[status]);
    return status;
}

//description of status
static char* const ws_decoder[] = {
  [WRITE_BMP_OK] = "BMP file successfully wrote\n",
  [WRITE_INVALID_INPUT] ="Error: filepath is wrong\n",
  [WRITE_ERROR] = "Error: something is wrong with writing\n"

};

//function for printing status
enum write_bmp_status print_bmp_ws(enum write_bmp_status status) {
    printf("%s", ws_decoder[status]);
    return status;
}

