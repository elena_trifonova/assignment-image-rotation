#ifndef IMAGE_FORMAT
#define IMAGE_FORMAT
#include <stdbool.h>
#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

void init_image(struct image* img, uint64_t width, uint64_t height);
void free_image(struct image* img);
#endif
